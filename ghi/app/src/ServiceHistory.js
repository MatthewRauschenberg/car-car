import React, { useEffect, useState } from "react";


function AppointmentHistory() {

    const [appointments, setAppointments] = useState([]);
    const [search, setSearch] = useState('');
    const [registeredVins, setRegisteredVins] = useState([]);


    const handleChange = async (e) => {
        setSearch(e.target.value);
    };

    const searchAppointments = async () => {
        const fetchUrl = `http://localhost:8080/api/appointments/`;
        const fetchResponse = await fetch(fetchUrl);

        if (fetchResponse.ok) {
            const fetchData = await fetchResponse.json();
            setAppointments(fetchData.appointments);
        }
    };
    const retrieveVins = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok){
            const data = await response.json();
            setRegisteredVins(data.autos.map(auto => auto.vin))
        } else {
            console.error(response);
        }
    }

    useEffect(() => {
        retrieveVins();
        searchAppointments();
    }, []);

    return (
        <>  <div className="gap-3 p-2 mt-5">
            <h2>Service History</h2>
            </div>
            <div className="p-4">
                <input
                    onChange={handleChange}
                    type="search"
                    placeholder="Search here"
                    className="form-control"
                    id="datatable-search-input">
                </input>
            </div>
            <table className="table table-striped shadow p-4 mt-2">
                <thead className="thead-dark">
                    <tr>
                        <th>Customer Name</th>
                        <th>Vin</th>
                        <th>Date & Time</th>
                        <th>Reason</th>
                        <th>Technician</th>
                        <th>Is Vip?</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter((appointment => {
                        return search === '' ? appointment : appointment.vin.includes(search)
                    })).map(appointment => {
                        const date = new Date(appointment.date_time)
                        const isVip = registeredVins.filter((registeredVin) => registeredVin == appointment.vin).length;
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.customer}</td>
                                <td>{appointment.vin}</td>
                                <td>{date.toLocaleString()}</td>
                                <td>{appointment.service_type}</td>
                                <td>{appointment.technician.first_name}</td>
                                <td>{isVip ? "Yes" : "No"}</td>
                                <td>{appointment.status ? "complete" : "canceled"}</td>

                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}
export default AppointmentHistory;

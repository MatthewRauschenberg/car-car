import React, {useEffect, useState} from 'react';

function SalesForm(){
    const[salespeople, setSalespeople] = useState([]);
    const[customers, setCustomers] = useState([]);
    const[automobiles, setAutomobiles] = useState([]);
    const[price, setPrice] = useState('');
    const[salesperson, setSalesperson] = useState('');
    const[customer, setCustomer] = useState('');
    const[automobile, setAutomobile] = useState('');

    const handleAutoSale = async(event, automobileVin) => {
        event.preventDefault();
        const url = `http://localhost:8100/api/automobiles/${automobileVin}/`
        const fetchConfig = {
            method: 'PUT',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({sold : true})
        }
        const response = await fetch(url, fetchConfig);
        if(response.ok){
            fetchData();
        }


    }

    const fetchData = async () => {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/'
        const salespeopleResponse = await fetch(salespeopleUrl);
        if (salespeopleResponse.ok) {
            const data = await salespeopleResponse.json();
            setSalespeople(data.salespeople);
        }

        const customersUrl = 'http://localhost:8090/api/customers/'
        const customersResponse = await fetch(customersUrl);
        if (customersResponse.ok) {
            const data = await customersResponse.json();
            setCustomers(data.customers);
        }

        const automobilesUrl = 'http://localhost:8100/api/automobiles/'
        const automobilesResponse = await fetch(automobilesUrl);
        if (automobilesResponse.ok) {
            const data = await automobilesResponse.json();
            setAutomobiles(data.autos);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }
    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data={}
        data.price = price;
        data.salesperson = salesperson;
        data.customer = customer;
        data.automobile = automobile;

        const url='http://localhost:8090/api/sales/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);

        if (response.ok){
            handleAutoSale(event,automobile);
            event.target.reset();
            setPrice('');
            setSalesperson('');
            setCustomer('');
            setAutomobile('');
        }

    }

    return(
        <div className="row">
            <div className = "offset-3 col-6">
                <div className = "shadow p-4 mt-4">
                    <h1>Create a New Sale:</h1>
                    <form onSubmit={(event) => handleSubmit(event)} id="create-sale-form">
                        <div className="form-floating mb-3">
                            <input onChange={handlePriceChange} placeholder="Pirce" required type="text" name="price" id="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select">
                                <option value="">Select a Salesperson</option>
                                {salespeople.map(salesperson => {
                                return (
                                    <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name + " " + salesperson.last_name}</option>
                                )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleCustomerChange} required name="customer" id="customer" className="form-select">
                                <option value="">Select a Customer</option>
                                {customers.map(customer => {
                                return (
                                    <option key={customer.id} value={customer.id}>{customer.first_name + " " + customer.last_name}</option>
                                )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleAutomobileChange} required name="automobile" id="automobile" className="form-select">
                                <option value="">Select an Automobile Vin</option>
                                {automobiles.filter(automobile => automobile.sold === false ).map(automobile => {
                                return (
                                    <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default SalesForm;

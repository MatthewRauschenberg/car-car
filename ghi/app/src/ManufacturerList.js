import React, {useEffect, useState} from 'react';

function ManufacturerList(){
    const [manufacturers, setManufacturers] = useState([]);

    const loadManufacturers = async () => {
        const response = await fetch ('http://localhost:8100/api/manufacturers');
        if (response.ok){
            const data = await response.json();
            setManufacturers(data.manufacturers);
        } else {
            console.error(response);
        }
    }

    useEffect(() => {
        loadManufacturers();
    }, []);

    return (
        <div>
            <br></br>
            <table className = "table table-striped table-hover">
                <thead className="table-dark">
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                        return(
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ManufacturerList;

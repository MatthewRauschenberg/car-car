import React, {useEffect, useState} from "react";

function SalespersonHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [sales, setSales] = useState([]);
    const [select, setSelect] = useState('');

    const fetchData = async () => {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
        const salespeopleResponse = await fetch(salespeopleUrl);
        if (salespeopleResponse.ok) {
            const data = await salespeopleResponse.json();
            setSalespeople(data.salespeople)
        }

        const salesUrl = 'http://localhost:8090/api/sales/';
        const salesResponse = await fetch(salesUrl);
        if (salesResponse.ok){
            const data = await salesResponse.json();
            setSales(data.sales);
        }
    }

    const handleSelectChange = (event) => {
        const value = event.target.value;
        setSelect(value);
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div>
            <h2>Get Salesperson History</h2>
            <div className = "mb-3">
                <select onChange={handleSelectChange} value={select} id="salesperson" name="salesperson" className="form-select">
                    <option value="">Choose a Salesperson</option>
                    {salespeople.map((salesperson) => {
                        return (
                            <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.first_name + " " + salesperson.last_name} </option>
                        )
                    })}
                </select>
            </div>
            <div>
                <table className="table table-striped table-hover">
                    <thead className="table-dark">
                        <tr>
                            <th>Salesperson</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sales.map((sale) => {
                            if (select !== "" && sale.salesperson.employee_id === select){
                            return(
                                <tr key ={sale.id}>
                                    <td>{sale.salesperson.first_name + " " + sale.salesperson.last_name}</td>
                                    <td>{sale.customer.first_name + " " + sale.customer.last_name}</td>
                                    <td>{sale.automobile.vin}</td>
                                    <td>{sale.price}</td>
                                </tr>
                            )
                            }
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default SalespersonHistory;

import React, {useEffect, useState} from 'react';

function SalespeopleList(){
    const [salespeople, setSalespeople] = useState([]);
    const handleDeleteSalesperson = async(event, salespersonId) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/salespeople/${salespersonId}`
        const fetchConfig = {
            method: "DELETE"
        }
        const response = await fetch(url, fetchConfig);
        if(response.ok){
            loadSalespeople();
        }
    }
    const loadSalespeople = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople');
        if (response.ok){
            const data = await response.json();
            setSalespeople(data.salespeople)
        } else {
            console.error(response);
        }
    }

    useEffect(() => {
        loadSalespeople();
    }, []);

    return (
        <div>
            <br></br>
            <table className="table table-striped table-hover">
                <thead className="table-dark">
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee Id</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => {
                        return (
                            <tr key={salesperson.id}>
                                <td>{salesperson.first_name}</td>
                                <td>{salesperson.last_name}</td>
                                <td>{salesperson.employee_id}</td>
                                <td><button type="button" className="btn btn-outline-danger" onClick={(event) => {
                                        handleDeleteSalesperson(event, salesperson.id)
                                    }}>Delete</button>
                                    </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )

}

export default SalespeopleList;

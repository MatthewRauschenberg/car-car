import React, {useEffect, useState} from 'react';


function AppointmentsForm(){
    const [technician, setTechnician] = useState([]);
    const [tech, setTech] = useState('');
    const [customer, setCustomer] = useState('');
    const [service_type, setService] = useState('');
    const [vin, setVin] = useState('')
    const [dateTime, setDateTime] = useState('');

    const loadTechs = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok){
          const data = await response.json();
          setTechnician(data.technicians)
        } else {
          console.error(response);
        }
    }

    useEffect(() => {
        loadTechs();
    }, []);

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleServiceChange = (event) => {
        const value = event.target.value;
        setService(value);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value)
    }

    const handleTechChange = (event) => {
        const value = event.target.value;
        setTech(value)
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data={}
        data.customer = customer;
        data.technician = tech;
        data.vin = vin;
        data.date_time = dateTime;
        data.service_type = service_type;

        const url = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        if(response.ok) {
            event.target.reset();
            setCustomer('');
            setTech('');
            setVin('');
            setDateTime('');
            setService('');


        }

    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a New Appointment?</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleCustomerChange} value={customer} placeholder="customer" required type="text" name="customer"
                                id="customer" className="form-control" />
                            <label htmlFor="name">Customer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" name="vin"
                                id="vin" className="form-control" />
                            <label htmlFor="style">Vin</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleDateTimeChange} value={dateTime} placeholder="YYYY-MM-DD HH:MM" required type="datetime" name="dateTime"
                                id="dateTime" className="form-control" />
                            <label htmlFor="dateTime">Date & Time</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleServiceChange} value={service_type} placeholder="serviceType" type="text" name="serviceType" id="serviceType"
                                className="form-control" />
                            <label htmlFor="serviceType">Reason</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleTechChange} value={tech} required id="tech" name="tech" className="form-select">
                                <option value="">Choose a Technician</option>
                                {technician.map(tech => {
                                    return (
                                        <option key={tech.first_name}>
                                            {tech.first_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create Appointment</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default AppointmentsForm;

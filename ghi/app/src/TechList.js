import React, {useEffect, useState} from "react";

function TechList() {
    const [techs, setTech] = useState([]);

    const loadTechs = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok){
          const data = await response.json();
          setTech(data.technicians)
        } else {
          console.error(response);
        }
    }

    useEffect(() => {
        loadTechs();
    }, []);

    return (
        <div>
            <br></br>
            <table className="table table-striped table-hover">
                <thead className="table-dark">
                    <tr>
                        <th>Tech First Name</th>
                        <th>Tech Last Name</th>
                        <th>Employee Id</th>

                    </tr>
                </thead>
                <tbody>
                    {techs.map(tech => {
                        return (
                            <tr key={tech.id}>
                                <td>{tech.first_name}</td>
                                <td>{tech.last_name}</td>
                                <td>{tech.employee_id}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default TechList;

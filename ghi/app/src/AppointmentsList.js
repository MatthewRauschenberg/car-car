import React, {useEffect, useState} from "react";

function AppointmentList(){
    const [appointments, setAppointments] = useState([]);
    const [registeredVins, setRegisteredVins] = useState([]);

    const retrieveVins = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok){
            const data = await response.json();
            setRegisteredVins(data.autos.map(auto => auto.vin))
        } else {
            console.error(response);
        }
    }

    const loadAppointments = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok){
            const data = await response.json();
            setAppointments(data.appointments)
        } else {
            console.error(response);
        }
    }

    useEffect(() => {
        loadAppointments();
        retrieveVins();
    }, []);

    const completeAppointment = async (id) => {
        const completeUrl = `http://localhost:8080/api/appointments/${id}/`;
        const fetchConfig = {
          method: "PUT",
          body: JSON.stringify({ "Completed": true }),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const completeResponse = await fetch(completeUrl, fetchConfig);
        if (completeResponse.ok) {
          loadAppointments();
        }
      };

      const cancelAppointment = async (id) => {
        const cancelUrl = `http://localhost:8080/api/appointments/${id}/`;
        const fetchConfig = {
          method: "DELETE",
          body: JSON.stringify(id),
          headers: {
            'Content-Type': 'application/json',
          }
        };
        const cancelResponse = await fetch(cancelUrl, fetchConfig);
        if (cancelResponse.ok) {
          loadAppointments();
        }
      };

    return (
        <div>
            <br></br>
            <table className="table table-striped table-hover">
                <thead className="table-dark">
                    <tr>
                        <th>Customer Name</th>
                        <th>Vin</th>
                        <th>Date & Time</th>
                        <th>Reason</th>
                        <th>Tech name</th>
                        <th>Is Vip?</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter((appointment) => {
                        return appointment.status === false;
                    }).map((appointment) => {
                    const date = new Date(appointment.date_time)
                    const isVip = registeredVins.filter((registeredVin) => registeredVin == appointment.vin).length;
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.customer}</td>
                                <td>{appointment.vin}</td>
                                <td>{date.toLocaleString()}</td>
                                <td>{appointment.service_type}</td>
                                <td>{appointment.technician.first_name}</td>
                                <td>{isVip ? "Yes" : "No"}</td>
                                <td>
                                    <button type="button" className="btn btn-success btn-xs" onClick={() => completeAppointment(appointment.id)}>Complete
                                    </button>
                                    <br></br>
                                    <button type="button" className="btn btn-danger btn-xs" onClick={() => cancelAppointment(appointment.id)}>Cancel
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
export default AppointmentList;

import React, {useEffect, useState} from 'react';

function CustomerList(){
    const [customers, setCustomers] = useState([]);
    const handleDeleteCustomer = async(event,customerId) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/customers/${customerId}`
        const fetchConfig = {
            method: "DELETE"
        }
        const response = await fetch(url, fetchConfig);
        if(response.ok){
            loadCustomers();
        }
    }
    const loadCustomers = async () => {
        const response = await fetch ('http://localhost:8090/api/customers');
        if (response.ok){
            const data = await response.json();
            setCustomers(data.customers);
        } else {
            console.error(response);
        }
    }

    useEffect(() => {
        loadCustomers();
    }, []);

    return (
        <div>
            <br></br>
            <table className = "table table-striped table-hover">
                <thead className="table-dark">
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(customer => {
                        return(
                            <tr key={customer.id}>
                                <td>{customer.first_name}</td>
                                <td>{customer.last_name}</td>
                                <td>{customer.address}</td>
                                <td>{customer.phone_number}</td>
                                <td><button type="button" className="btn btn-outline-danger" onClick={(event) => {
                                        handleDeleteCustomer(event, customer.id)
                                    }}>Delete</button>
                                    </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default CustomerList;
